import * as React from 'react';

import {  Typography, Box } from '@mui/material';

interface letter {
    letter: string;
    color: string;
    police?: string;
    // size?: number;
}

const letters: letter[] = [
    {
        letter: 'A',
        color: 'red',
        police: 'Climate Crisis',
    },
    {
        letter: 'S',
        color: 'blue',
        police: 'Climate Crisis',
    },
    {   
        letter: 'T',
        color: 'green',
        police: 'Climate Crisis',
    },
    {
        letter: 'R',
        color: 'yellow',
        police: 'Climate Crisis',
    },
    {
        letter: 'O',
        color: 'purple',
        police: 'Climate Crisis',
    },
    {
        letter: '-',
        color: 'orange',
        police: 'Climate Crisis',
    },
    {
        letter: 'S',
        color: 'red',
        police: 'Climate Crisis',
    },
    {
        letter: 'H',
        color: 'blue',
        police: 'Climate Crisis',
    },
    {
        letter: 'O',

        color: 'green',
        police: 'Climate Crisis',
    },
    {
        letter: 'P',
        color: 'yellow',
        police: 'Climate Crisis',
    }
]

interface Props {
    orientation?: 'horizontal' | 'vertical';
    size?: number;
    variant?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'subtitle1' | 'subtitle2' | 'body1' | 'body2' | 'button' | 'caption' | 'overline' | 'inherit';
    sx?: any;
}


export default class Title extends React.Component<Props> {

    render() {
        return (
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: this.props.orientation || 'horizontal',
                    alignItems: 'center',
                    justifyContent: 'center',
                    // width: 300,
                    
                } 
                    && this.props.sx
            }
            >
                {letters.map((letter, index) => {
                return (
                    <Typography
                        key={index}
                        variant={this.props.variant}
                        sx={{
                            color: letter.color,
                            fontSize: this.props.size,
                            fontFamily: letter.police,
                            display: 'inline',
                        }}
                        
                    >
                        {letter.letter}
                    </Typography>
                )
            })}
            </Box>
        );
    }
}
