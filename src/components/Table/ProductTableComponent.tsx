import * as React from 'react';
import Product from '../../types/Product';
import { Table, TableRow, TableHead, TableBody, TableCell } from '@mui/material';

interface HeaderElement {
    id: number;
    name: string;
    align: 'center' | 'right' | 'left';
}




const headerElements : Array<HeaderElement> = [
    {
        id: 1,
        name: 'name',
        align: 'center',
    },
    {
        id: 2,
        name: 'Price ($)',
        align: 'center',
    },
    {
        id: 3,
        name: 'Quantity',
        align: 'center',
    },
    
];


export default class ProductTable extends React.Component<{products: Array<Product>}> {
    render() {
        return (
            <Table size='small'>
                <TableHead>
                    <TableRow>
                        {headerElements.map((element) => (
                            <TableCell key={element.id} align={element.align}>
                                {element.name}
                            </TableCell>))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.props.products.map((product) => (
                        <TableRow key={product.name}>
                            <TableCell
                                align='center'
                            >{product.name}</TableCell>
                            <TableCell
                                align='center'
                            >{product.price}</TableCell>
                            <TableCell
                                align='center'
                            >{product.quantity}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        );
    }
}