import * as React from 'react';
import { Checkbox }  from '@mui/material';
import { blue } from '@mui/material/colors';

import {  TableCell, TableRow } from '@mui/material';
interface HeaderElement {
    label: React.ReactNode;
    align?: 'left' | 'right' | 'center';
    width?: number|string;
    padding?:"none" | "checkbox" | "normal" | undefined
    
}


const HedearsElements: Array<HeaderElement> = [
    {label: '', align: 'left', width: 2, padding: 'none'},
    {label: <Checkbox color="secondary"></Checkbox>, align: 'left', width: 2, padding: 'checkbox'},
    {label: 'number', align: 'left', width: 2},
    {label: 'date', align: 'left', width: 2},
    {label: 'total price', align: 'left', width: 8},
    {label: 'commands', align: 'left', width: 8},
];
    
export default class HeaderTable extends React.Component {
    render() {
        return (
            <TableRow
                sx={{
                    
                    color: 'primary.contrastText',
                }}
            >
                {HedearsElements.map((headerElement, idx) => (
                    <TableCell
                        key={idx}
                        align={headerElement.align}
                        sx={{
                            backgroundColor: blue[500],
                            color: 'white',
                            width: headerElement.width,
                            padding: headerElement.padding,
                        }}
                    >
                        {headerElement.label}
                    </TableCell>
                ))}
            </TableRow>

        );
    }
} 