import * as React from 'react';
import {KeyboardArrowDown, KeyboardArrowUp} from "@mui/icons-material";

import { Edit, Delete, Add, Remove } from '@mui/icons-material';
import ProductTable from './ProductTableComponent';

import {
    TableRow,
    TableCell,
    IconButton,
    Collapse,
    Checkbox,
    Dialog,
    DialogTitle,
    DialogContent,
    Box,
    Typography,
    DialogActions,
    Button,
    TextField,
} from '@mui/material';
import Order from '../../types/Order';


interface Props {
    order: Order;
    sx?: any;
    removeOrder: (id: string) => void;
    editOrder: (order: Order ) => void;
    // updateOrder: (id: string, order: Order) => void;
}

interface States {
    open: boolean;
    openEditMenu: boolean;
    editedOrder: Order;
    numberOfProducts: number;
    dateInput: string;
}


interface RowElement {
    label: React.ReactNode | Date ;
    align?: 'left' | 'right' | 'center';
    width?: number|string;
    padding?:"none" | "checkbox" | "normal" | undefined
}


export default class RowComponent extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props);
        this.state = {
            open: false,
            openEditMenu: false,
            editedOrder: {
                id: this.props.order.id,
                orderNumber: this.props.order.orderNumber,
                orderDate: this.props.order.orderDate,
                products: this.props.order.products,
            },
            numberOfProducts: this.props.order.products.length,
            dateInput: `${this.props.order.orderDate.getFullYear()}-${this.props.order.orderDate.getMonth() + 1 > 9  ? this.props.order.orderDate.getMonth() + 1: "0"+(this.props.order.orderDate.getMonth() + 1)}-${this.props.order.orderDate.getDate()}`
        };
    }

    handleTextFieldChange = (event: React.ChangeEvent<HTMLInputElement> ) => {
        const { name, value } = event.target;
        this.setState({ editedOrder: { ...this.state.editedOrder, [name]: value } });
    }

    handleTextFieldChangeDate = (event: React.ChangeEvent<any>) => {
        this.setState({ dateInput: event.target.value });
        this.setState({ editedOrder: { ...this.state.editedOrder, orderDate: new Date(event.target.value) } });
    }




    handleTextFieldChangeProduct = (event: React.ChangeEvent<any>, idx: Number) => {
        const { name, value } = event.target;
        const [type, index] = name.split("-");
        this.setState((prevState) => ({
            editedOrder: {
                ...prevState.editedOrder,
                products: prevState.editedOrder.products.map((product, i) => {
                    if (i === idx) {
                        return {
                            ...product,
                            [type]: value,
                        };
                    }
                    return product;
                }),
            },
        }));
    }

    totalProductsPrice() {
        let total = 0;
        this.props.order.products.forEach((product) => {
            total += product.price * product.quantity;
        });
        return total;
    }

    render() {
        const rowElements: Array<RowElement> = [
            {label: <IconButton onClick={() => this.setState({open: !this.state.open}) }>{this.state.open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}</IconButton>, padding: "none"},
            {label: <Checkbox color="secondary"></Checkbox>, padding: "checkbox"},
            {label: this.props.order.orderNumber},
            {label: this.props.order.orderDate},
            {label: this.totalProductsPrice()},
            {label: <>
                <IconButton 
                    onClick={() => this.setState({openEditMenu: true, 
                        numberOfProducts: this.props.order.products.length
                    })}
                ><Edit /></IconButton>
                <IconButton onClick={() => this.props.removeOrder(this.props.order.id)}  ><Delete /></IconButton>
            </>, padding: "none"},
        ];

        return (
        <>
            <TableRow>
                {rowElements.map((rowElement, idx) => (
                    <TableCell
                        key={idx}
                        align={rowElement.align}
                        sx={{
                            width: rowElement.width,
                            padding: rowElement.padding,
                        }  && this.props.sx}
                    >   
                        {
                            rowElement.label instanceof Date ? rowElement.label.toLocaleDateString('en-GB', { timeZone: 'UTC' }) : rowElement.label
                        }
                        {/* {typeof rowElement.label === 'object' ? new Date(rowElement.label).toLocaleDateString() : rowElement.label} */}
                    </TableCell>
                ))}
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={2}/>
                <TableCell sx={{padding: 0}} colSpan={6}>
                    <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                        <ProductTable products={this.props.order.products} />
                    </Collapse>
                </TableCell>
            </TableRow>
            <Dialog open={this.state.openEditMenu} onClose={() => this.setState({openEditMenu: false})}>
                <DialogTitle>Edit Order</DialogTitle>
                <DialogContent>
                <Box sx={{p: 2}}>
                                <TextField label="Order number" variant="filled"
                                // error={this.state.addOrder.orderNumber === ""}
                                name='orderNumber'
                                required
                                // helperText="Incorrect entry."
                                fullWidth value={this.state.editedOrder.orderNumber} />
                                <TextField label="Order date" 
                                helperText="default date is today"
                                name='orderDate'
                                required
                                onChange={this.handleTextFieldChangeDate}
                                    sx={{mt: 2}}
                                    InputLabelProps={{
                                        shrink: true,
                                      }}
                                type={"date"} variant="outlined" fullWidth value={this.state.dateInput} />
                                <Box
                                sx={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    p: 2
                                }}

                                >
                                <Typography variant="h6">Products</Typography> 
                                <Box>
                                <IconButton 
                                 onClick={() =>
                                    this.setState((prevState) => ({
                                        numberOfProducts: prevState.numberOfProducts + 1,
                                        editedOrder: {
                                            ...prevState.editedOrder,
                                            products: [
                                                ...prevState.editedOrder.products,
                                                {
                                                    name: "product "+prevState.numberOfProducts,
                                                    price: 0,
                                                    quantity: 0,
                                                },
                                            ],
                                        },
                                    }))
                                }
                                >
                                    <Add />
                                </IconButton>
                                <IconButton
                                onClick={() =>
                                    {if(this.state.numberOfProducts > 0) {
                                        this.setState((prevState) => ({
                                            numberOfProducts: prevState.numberOfProducts - 1,
                                            editedOrder: {
                                                ...prevState.editedOrder,
                                                products: prevState.editedOrder.products.slice(0, -1),
                                            },
                                        }));
                                    }}
                                }
                                
                                >
                                    <Remove />
                                </IconButton>
                                </Box>
                                </Box>
                                {
                                    this.state.numberOfProducts > 0 ?
                                    Array.from(Array(this.state.numberOfProducts).keys()).map((idx) => (
                                        <Box key={idx} sx={{display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between", p: 2}}>
                                            <TextField 
                                            required
                                            name={`name-${idx}`}
                                            onChange={(e) => this.handleTextFieldChangeProduct(e, idx)} label="Product name" variant="outlined" fullWidth value={this.state.editedOrder.products[idx].name} />
                                            <TextField 
                                            required
                                            name={`price-${idx}`}
                                            onChange={(e) => this.handleTextFieldChangeProduct(e, idx)} label="Price" variant="outlined" fullWidth value={this.state.editedOrder.products[idx].price} />
                                            <TextField 
                                            required
                                            name={`quantity-${idx}`}
                                            onChange={(e) => this.handleTextFieldChangeProduct(e, idx)} label="Quantity" variant="outlined" fullWidth value={this.state.editedOrder.products[idx].quantity} />
                                        </Box>
                                    )) : null
                                    
                                }
                            </Box>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.setState({openEditMenu: false,
                    numberOfProducts: 0
                    })}>Cancel</Button>
                    <Button onClick={() => {
                        this.props.editOrder(this.state.editedOrder);
                        this.setState({openEditMenu: false,
                        numberOfProducts: 0                        })}}>Save</Button>
                </DialogActions>
            </Dialog>

            
        </>
        );
    }
}