import Product from './Product';

export default interface Order {
    id: string;
    orderNumber: string;
    orderDate: Date;
    products: Product[];
}