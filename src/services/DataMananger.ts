
import axios from "axios";
import Order from "../types/Order";
// import Order from "../types/Order";
// import User from "../types/User";
import Product from "../types/Product";

const API_URL = "http://localhost:8000/api";


export const signUp = (firstname: string, lastname: string, email: string, password: string):Promise<any> => {
  return axios.post(`${API_URL}/users/register`, 
    { firstname, lastname, email, password });
};

export const signIn = (email: string, password: string):Promise<any> => {
    return axios.post(`${API_URL}/users/login`, { email, password });
}

export const getOrders = ():Promise<any> => {
    return axios.get(`${API_URL}/orders`);
}

export const createOrder = (order: Order):Promise<any> => {

    const { orderNumber, orderDate, products } = order;

    const prods = products.map((product: Product) => {
        const { name, quantity, price } = product;
        return { name, quantity, price };
    });


    return axios.post(`${API_URL}/orders`, { 
        number: orderNumber,
        date: orderDate, 
        products: prods 
    });

}

export const deleteOrder = (id: string):Promise<any> => {
    return axios.delete(`${API_URL}/orders/${id}`);
}


export const updateOrder = (order: Order):Promise<any> => {

    const { orderNumber, orderDate, products } = order;
    const prods = products.map((product: Product) => {
        const { name, quantity, price } = product;
        return { name, quantity, price };
    });

    return axios.put(`${API_URL}/orders/${order.id}`, {
        number: orderNumber,
        date: orderDate,
        products: prods
    });
    
}
