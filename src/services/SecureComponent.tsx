import * as React from 'react';
import { Navigate, Outlet } from 'react-router-dom';




export default class SecureComponent extends React.Component {

    render() {
        if (localStorage.getItem('token')) {
            return (
                <Outlet />
            );
        } else {
            return (
                <Navigate to="/" />
            );
        }
    }
}