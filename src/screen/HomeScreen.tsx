import * as React from 'react';
import { Table, TableBody, TableHead, SpeedDial, SpeedDialAction, SpeedDialIcon, Dialog, DialogTitle, DialogContent, DialogActions, TextField, Button, Box, Typography, IconButton } from '@mui/material';
import { Add, Remove } from '@mui/icons-material';
import uuid from 'react-uuid';
// import { getOrders } from '../services/DataMananger';
import Order from '../types/Order';
import HeaderTable from '../components/Table/HeaderTableComponent';
import RowComponent from '../components/Table/RowComponent';
import { deleteOrder, createOrder, getOrders, updateOrder } from '../../src/services/DataMananger';
interface Props {
}

interface States {
    orders: Order[];
    open: boolean;
    addOrder: Order;
    numberOfProducts: number;
    openEditOrder: boolean;
    openAddOrder: boolean;
    dateInput: string;
}



const actions = [
    { icon: <Add />, name: 'Add' },
    // { icon: <Filter />, name: 'Filter' },
];

export default class HomeScreen extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props);
        this.state = {
            orders: [],
            addOrder: 
            {
                id: "",
                orderNumber: "",
                orderDate: new Date(),
                products: []
            },
            open: false,
            numberOfProducts: 0,
            openEditOrder: false,
            openAddOrder: false,
            dateInput: ""
        };
    }



    handleTextFieldChange = (event: React.ChangeEvent<HTMLInputElement> ) => {
        const { name, value } = event.target;
        this.setState({ addOrder: { ...this.state.addOrder, [name]: value } });
    }

    handleTextFieldChangeDate = (event: React.ChangeEvent<any>) => {
        this.setState({ dateInput: event.target.value });
        this.setState({ addOrder: { ...this.state.addOrder, orderDate: new Date(event.target.value) } });
    }




    handleRemoveOrder = (id: string) => {
        deleteOrder(id).then((order: any) => {
            this.setState({ orders: this.state.orders.filter((order) => order.id !== id) });
        }).catch((error) => {
            console.log(error);
        });
        // this.setState({ orders: this.state.orders.filter((order) => order.id !== id) });
    }

    handleTextFieldChangeProduct = (event: React.ChangeEvent<any>, idx: Number) => {
        const { name, value } = event.target;
        const [type, index] = name.split("-");
        this.setState((prevState) => ({
            addOrder: {
                ...prevState.addOrder,
                products: prevState.addOrder.products.map((product, i) => {
                    if (i === idx) {
                        return {
                            ...product,
                            [type]: value,
                        };
                    }
                    return product;
                }),
            },
        }));
    }

    // handleUpdateProduct = (event: React.ChangeEvent<any>, order: Order ) => {
    //     updateOrder(order).then(() =>
    //             this.setState({ orders: this.state.orders.map((order) => {
    //     )
    // }




    componentDidMount() {   
        getOrders().then((orders:any ) => {

            // console.log(orders.data);
            
            const newOrders: Order[] = orders.data.map((order: any) => {
                const { _id, number, products, date } = order;
                if(products.length > 0) {
                    const newProducts = products.map((product: any) => {
                        const { price, name, quantity } = product;
                        return {
                            price: price,
                            name: name,
                            quantity: quantity,
                        };
                    });
                    return {
                        id: _id,
                        orderNumber: number,
                        orderDate: new Date(date),
                        products: newProducts,
                    };
                }else 
                {
                    return {
                        id: _id,
                        orderNumber: number,
                        orderDate: new Date(date),
                        products: [],
                    };
                }
            });
            this.setState({ orders: newOrders });
            

        }).catch((error) => {
            console.log(error);
        });
        
    }

    

    handleAddOrder = (event:  React.MouseEvent<any>) => {
        //  this.setState({openAddOrder: false, orders: this.state.orders.concat(this.state.addOrder) })
        
        createOrder(this.state.addOrder).then((order: any) => {
            const { _id, number, products, date } = order.data;
            if(products.length > 0) {
                const newProducts = products.map((product: any) => {
                    const { price, name, quantity } = product;
                    return {
                        price: price,
                        name: name,
                        quantity: quantity,
                    };
                });
                const newOrder = {
                    id: _id,
                    orderNumber: number,
                    orderDate: new Date(date),
                    products: newProducts,
                };
            
            
            this.setState({ orders: [...this.state.orders, newOrder] });
            }else {
                const newOrder = {
                    id: _id,
                    orderNumber: number,
                    orderDate: new Date(date),
                    products: [],
                };
                
                this.setState({ orders: [...this.state.orders, newOrder],
                    // numberOfProducts: 0,
                
                });
            }


        }).catch((error) => {
            console.log(error);
        }
        );
        this.setState({ openAddOrder: false});
        this.setState({  numberOfProducts: 0});
        console.log(this.state.numberOfProducts);
        
    }

    handleUpdateOrder = (order: Order) => {
        updateOrder(order).then((res) => {
            this.setState({ orders: this.state.orders.map((order) => {
                if(order.id === res.data._id) {
                    return {
                        id: res.data._id,
                        orderNumber: res.data.number,
                        orderDate: new Date(res.data.date),
                        products: res.data.products,
                    };
                }
                return order;
            }) });
        }).catch((error) => {
            console.log(error);
        }
        );

    }

    render() {
        return (
                <>
                    <Table sx={{position: "relative"}} stickyHeader >
                        <TableHead>
                            <HeaderTable />
                        </TableHead>
                        <TableBody>
                            {
                                this.state.orders.map((order, idx) => (
                                    <RowComponent key={idx} order={order} sx={{backgroundColor: idx % 2 === 0 ? "white" : "grey.100"}} removeOrder={this.handleRemoveOrder}
                                        editOrder={this.handleUpdateOrder}
                                    />
                                ))
                            }
                        </TableBody>
                    </Table>
                    <SpeedDial
                        ariaLabel="SpeedDial openIcon example"
                        sx={{ position: 'fixed', bottom: 16, left: 16 }}
                        icon={<SpeedDialIcon />}
                        onClose={() => this.setState({open: false})}
                        onOpen={() => this.setState({open: true})}
                        open={this.state.open}
                        direction="up"
                    >
                        {actions.map((action) => (
                            <SpeedDialAction

                                key={action.name}
                                icon={action.icon}
                                tooltipTitle={action.name}
                                onClick={() => this.setState({openAddOrder: !this.state.openAddOrder, 
                                    addOrder: {
                                        id: "",
                                        orderNumber: uuid(),
                                        orderDate: new Date(),
                                        products: []
                                    },
                                })}
                            />
                        ))}
                    </SpeedDial>
                    <Dialog open={this.state.openAddOrder} onClose={() => this.setState({openAddOrder: false})}>
                        
                        <DialogTitle>Add order</DialogTitle>
                        <DialogContent>
                            <Box sx={{p: 2}}>
                                <TextField label="Order number" variant="filled"
                                // error={this.state.addOrder.orderNumber === ""}
                                name='orderNumber'
                                required
                                // helperText="Incorrect entry."
                                fullWidth value={this.state.addOrder.orderNumber} />
                                <TextField label="Order date" 
                                helperText="default date is today"
                                name='orderDate'
                                required
                                onChange={this.handleTextFieldChangeDate}
                                    sx={{mt: 2}}
                                    InputLabelProps={{
                                        shrink: true,
                                      }}
                                type={"date"} variant="outlined" fullWidth value={this.state.dateInput} />
                                <Box
                                sx={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    p: 2
                                }}

                                >
                                <Typography variant="h6">Products</Typography> 
                                <Box>
                                <IconButton 
                                 onClick={() =>
                                    this.setState((prevState) => ({
                                        numberOfProducts: prevState.numberOfProducts + 1,
                                        addOrder: {
                                            ...prevState.addOrder,
                                            products: [
                                                ...prevState.addOrder.products,
                                                {
                                                    name: "product "+prevState.numberOfProducts,
                                                    price: 0,
                                                    quantity: 0,
                                                },
                                            ],
                                        },
                                    }))
                                }
                                >
                                    <Add />
                                </IconButton>
                                <IconButton
                                onClick={() =>
                                    {if(this.state.numberOfProducts > 0) {
                                        this.setState((prevState) => ({
                                            numberOfProducts: prevState.numberOfProducts - 1,
                                            addOrder: {
                                                ...prevState.addOrder,
                                                products: prevState.addOrder.products.slice(0, -1),
                                            },
                                        }));
                                    }}
                                }
                                
                                >
                                    <Remove />
                                </IconButton>
                                </Box>
                                </Box>
                                {
                                    this.state.numberOfProducts > 0 ?
                                    Array.from(Array(this.state.numberOfProducts).keys()).map((idx) => (
                                        <Box key={idx} sx={{display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "space-between", p: 2}}>
                                            <TextField 
                                            required
                                            name={`name-${idx}`}
                                            onChange={(e) => this.handleTextFieldChangeProduct(e, idx)} label="Product name" variant="outlined" fullWidth value={this.state.addOrder.products[idx].name} />
                                            <TextField 
                                            required
                                            name={`price-${idx}`}
                                            onChange={(e) => this.handleTextFieldChangeProduct(e, idx)} label="Price" variant="outlined" fullWidth value={this.state.addOrder.products[idx].price} />
                                            <TextField 
                                            required
                                            name={`quantity-${idx}`}
                                            onChange={(e) => this.handleTextFieldChangeProduct(e, idx)} label="Quantity" variant="outlined" fullWidth value={this.state.addOrder.products[idx].quantity} />
                                        </Box>
                                    )) : null
                                    
                                }
                            </Box>
                            
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => this.setState({openAddOrder: false,
                                    numberOfProducts: 0,
                            
                            })}>
                                Cancel
                            </Button>
                            <Button onClick={this.handleAddOrder}>Add</Button>
                        </DialogActions>
                    </Dialog>
                    
                    
                </>
            
        );
    }
}
