import { Paper, CircularProgress } from '@mui/material';
import * as React from 'react';
import { Navigate   } from 'react-router-dom';
import Title from '../components/TitleComponent';
interface Props {
    // children: React.ReactNode;
    
}

interface State {
    idToken: string | null ;
    loading: boolean;
}


export default class SplashScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            idToken: null,
            loading: true,
        };
    }

    componentDidMount() {    
        
       
            this.setState({ idToken: localStorage.getItem('idToken') });
            this.setState({ loading: true });
            setTimeout(() => {
                this.setState({ loading: false });
            }
            , 1000);
    }
    
    render() 
    {
        if (this.state.loading) {
            return (
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100vh',
                    flexDirection: 'column',

                }}>
                    <Title variant='h2'  />
                    <CircularProgress />
                </div>
            );
        }
        if (this.state.idToken) {
            return (
                <Navigate to="/home" />
            );
        } else {
            return (
                <Navigate to="/login" />
            );
        }
    }
    }

    