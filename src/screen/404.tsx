import { Paper, Typography } from '@mui/material';
import * as React from 'react';
import image from '../static/images/404.png';


export default class NoPageFound extends React.Component {
    render() {
        return (
        <Paper
            sx={{
                height: '100vh',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 3,
                textAlign: 'center',
            }}

        >
            <Typography variant="h1" component="div"gutterBottom
                
            >
                Error 404: This page does not exist in this universe
            </Typography>
            <img 
            style={{
                width: 300,
                height: 300,
            }}


            src={image} alt="logo"/>
            <Typography variant="h4" component="div" gutterBottom>
                go back to <a href="/">home</a>
            </Typography>

        </Paper>
        );
    }
}