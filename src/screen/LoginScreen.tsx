import { Button, Card, TextField, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import image from '../static/images/shoping_on_moon.png';
import { Navigate } from 'react-router-dom';
import { Box } from '@mui/system';
import * as    React from 'react';
import { Link } from 'react-router-dom';
import Title from '../components/TitleComponent';
import { signIn } from '../services/DataMananger';



const style = {

  input: {
    width: '100%',
    marginBottom: 3,
  },
  button: {
    width: '100%',
    // marginTop: 10,
  },
  paper: {
    
      height: '100vh',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',

      // justifyContent: 'center',
      // alignItems: 'center',

    }

};


interface Props {
  // children: React.ReactNode;

}

interface State {
  email: string;
  password: string;
  canNavigate: boolean;
  // loading: boolean;
}


export default class LoginScreen extends React.Component< Props, State> {
  
  constructor(props: Props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      canNavigate: false,
      // loading: true,
    };
  }

  handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ email: event.target.value });
  };

  handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ password: event.target.value });
  };

  handleSubmit = (event: React.FormEvent<HTMLButtonElement>) => {
    event.preventDefault();
    signIn(this.state.email, this.state.password).then((result) => {
      if (result) {
        localStorage.setItem('token', result.id);
        this.setState({ canNavigate: true });
      }
    }).catch((error) => {
      console.log(error);
    }
    );
  }

  render() {
    return (
        <Paper sx={
          style.paper
        }>
          <Title variant='h3'  />
        
          <Card sx={{ width: 300, padding: 3, marginTop: 3}}>
          
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              width: 300,
              alignItems: 'center',
            }}
              component="form"
            >
            <img src={image} alt="logo"
            style={{

              width: 100,
              justifySelf: 'center',
              // height: 300,
              marginBottom: 20,
            }}
          />
          <Typography variant="h4" component="div" gutterBottom>
            Login
          </Typography>
          <Typography variant="h6" component="div" gutterBottom>
            Welcome back!
          </Typography>

            <TextField
              required   
              id="email"
              value={this.state.email}
              onChange={this.handleEmailChange}
            label="email" sx={style.input} />
            <TextField label="Password" sx={style.input} 
            required
            type="password"
            id="password"
            onChange={this.handlePasswordChange}
            value={this.state.password}
            />
            <Button variant="contained" 
            type="submit"
            onClick={this.handleSubmit}
            sx={style.button}>Login</Button>
            {this.state.canNavigate && <Navigate to="/home"  replace={true} />}
          </Box>
          </Card>
          <Typography variant="h6" component="div" gutterBottom>
            Don't have an account? <Link to={"/signUp"} >Sign up</Link>
          </Typography>
        </Paper>
    );
  }
}