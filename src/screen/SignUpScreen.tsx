import * as React from 'react';

import { Button, Card, TextField, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import image from '../static/images/astro_signup.png';
import { Box } from '@mui/system';
import { Link } from 'react-router-dom';
import { signUp } from '../services/DataMananger';
import { Navigate } from 'react-router-dom';
import Title from '../components/TitleComponent';

const style = {

input: {
    width: '100%',
    marginBottom: 3,
},
button: {
    width: '100%',
    // marginTop: 10,
},
paper: {
    
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,

    // justifyContent: 'center',
    // alignItems: 'center',

    }

};


interface State {
email: string;
password: string;
firstname: string;
lastname: string;
canNavigate: boolean;
// loading: boolean;
}


export default class SignUpScreen extends React.Component<{

}, State>  {


constructor(props: {}) {
super(props);
this.state = {
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    canNavigate: false,
    
    // loading: true,
};
this.handleSignUp = this.handleSignUp.bind(this);
}

handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
this.setState({ email: event.target.value });
}

handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
this.setState({ password: event.target.value });
}

handleFirstnameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
this.setState({ firstname: event.target.value });
}

handleLastnameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
this.setState({ lastname: event.target.value });
}

handleSignUp = (e: React.MouseEvent<HTMLButtonElement>) => { 
    e.preventDefault();
    signUp(
        this.state.firstname,
        this.state.lastname,
        this.state.email,
        this.state.password,
    )
    .then((res) => {
        // console.log(res);
        localStorage.setItem('token', res.id.token);
        this.setState({canNavigate: true});
    })
    .catch((err) => {
        console.error(err);
    }
    );



}

render() {
return (
    <Paper sx={
        style.paper
    }>

        <Title variant='h3'  />

        <Card sx={{ width: 300, padding:  3, marginTop: 3 }}>
        
        <Box
        sx={{
            display: 'flex',
            flexDirection: 'column',
            // width: 300,
            alignItems: 'center',
            
        }}
            component="form"
        >
        <img src={image} alt="logo"
        style={{

            width: 100,
            justifySelf: 'center',
            // height: 300,
            marginBottom: 20,
        }}
        />
        <Typography variant="h4" component="div" gutterBottom>
            Sign Up
        </Typography>
        
        <Typography variant="h6" component="div" gutterBottom>
        Welcome to Astro Shop
        </Typography>
        <TextField 
        value={this.state.email}
        type={"email"} 
        label="email" 
        sx={style.input} 
        onChange={this.handleEmailChange}
        />
        <TextField 
        value={this.state.firstname}
        type={"text"} 
        label="firstname" 
        sx={style.input} 
        onChange={this.handleFirstnameChange}
        />
        <TextField 
        value={this.state.lastname}
        type={"text"} 
        label="lastname" 
        sx={style.input}
        onChange={this.handleLastnameChange}
        
        />
        <TextField 
        value={this.state.password}
        type={"password"} label="Password" 
        sx={style.input} 
        onChange={this.handlePasswordChange}
        />
        <Button variant="contained"
        
        onClick={
             this.handleSignUp 
        }
        sx={style.button}>Sign Up</Button>
        {this.state.canNavigate && <Navigate to="/" />}
        </Box>
        </Card>
        <Typography variant="h6" component="div" gutterBottom>
        Already have an account? <Link to="/login">Login</Link>
        </Typography>
    </Paper>
);
}
}

