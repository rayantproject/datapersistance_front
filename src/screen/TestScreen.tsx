import * as React from "react";
import {
  Table,
  TableBody,
  TableHead,
  SpeedDial,
  SpeedDialAction,
  SpeedDialIcon,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Button,
  Box,
  Typography,
  IconButton
} from "@mui/material";

interface State {
  date: string;
  dateOn: Date;
}

export default class Test extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      dateOn: new Date(Date.now()),
      date: ""
    };
  }

  handleTextInput = (e: React.ChangeEvent<any>) => {
    this.setState({
      date: e.target.value,
      dateOn: new Date(e.target.value)
    
    });
  };

  render() {
    const { date, dateOn } = this.state;
    return (
      <>
        <TextField type="date" value={date} onChange={this.handleTextInput} />

        <Typography>{date}</Typography>
        <Typography>{dateOn.toLocaleDateString()}</Typography>
      </>
    );
  }
}
