import React from 'react';
import { BrowserRouter,  Route, Routes } from "react-router-dom";
import Login from './screen/LoginScreen';
import SignUp from './screen/SignUpScreen';
import Home from './screen/HomeScreen';
import NoPageFound from './screen/404';
// import Title from './components/TitleComponent';
import SecureComponent from './services/SecureComponent';
import SplashScreen from './screen/SplashScreen';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path='/'  >
            <Route index element={<SplashScreen />} />
            <Route path="/login" element={<Login />} />
            <Route path="/SignUp" element={<SignUp />} />	
          </Route>
          <Route path="/home" element={<SecureComponent />} >
            <Route index element={<Home />} />
          </Route>

      
            <Route path="*" element={<NoPageFound/>} />
        </Routes>

      </BrowserRouter>
    );
  }
}
export default App;
